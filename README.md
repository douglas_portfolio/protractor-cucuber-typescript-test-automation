[Instructions](#instructions)<br />

This project makes the test automation of the [Way2Automation WebTables page](http://www.way2automation.com/angularjs-protractor/webtables/).

[Frameworks Used](#frameworksUsed)

- Protractor (as webdriver)
- Cucumber (as BDD)
- Chai (to increment validation)

The project is all developed in NodeJs.

[Instructions to Execute](#instructionsToExecute)

This project implements the following scripts for node pipeline:

- build
- clean
- clean-build
- init-contributors
- generate-contributors
- test
- webdriver-update
- webdriver-start

First of all you need to import the node dependencies. So, after download this project run:

```npm install```

After that, run in this sequence:

- `node run clean-build`
- `node test`

The test will print an exception which is expected due to the last validation throws this exception.
The test will generate the reports in the folder:

```./reports```

They are in `json` and `html`.
