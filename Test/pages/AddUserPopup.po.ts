import {$, browser, element, by, ElementFinder, ExpectedConditions, until} from "protractor";
import {String, StringBuilder} from 'typescript-string-operations';
import {selectDropdownbyNum} from "../hooks/hooks";
import construct = Reflect.construct;


export class AddUserPopupPo {

    private inputFieldName: string;
    private saveButton: ElementFinder;
    private customerRadioButton: string;
    private roleDropDown: ElementFinder;
    private createdUser: User;

    constructor() {
        this.inputFieldName = "input[name={0}]";
        this.saveButton = element(by.xpath("//button[text()='Save']"))
        this.customerRadioButton = "//label[text()='{0}']//child::input";
        this.roleDropDown = $("select[name='RoleId']")
        this.createdUser = new User();
    }

    get user() {
        return this.createdUser;
    }

    fillInputWithName(inputFieldName: InputFieldName, data: string): AddUserPopupPo {
        $(String.Format(this.inputFieldName, inputFieldName.valueOf())).sendKeys(data);
        this.createdUser.set(inputFieldName, data);
        return this;
    }

    clickOnSave() {
        this.saveButton.click();
    }

    clickOnChosenCustomer(customer: string): AddUserPopupPo {
        element(by.xpath(String.Format(this.customerRadioButton, customer))).click();
        this.createdUser.customer = customer;
        return this;
    }

    selectRole(role: any): AddUserPopupPo {
        selectDropdownbyNum(this.roleDropDown, role);
        this.createdUser.role = role;
        return this;
    }

    isLoaded(): boolean {
        let isLoaded: boolean = true;
        try {
            browser.wait(ExpectedConditions.visibilityOf(this.saveButton));
        } catch (e) {
            isLoaded = false;
        }

        return isLoaded;
    }
}

export enum RoleDropDownIndex {
    SALES_TEAM = 0,
    CUSTOMER = 1,
    ADMIN = 2
}

export enum InputFieldName {
    FIRST_NAME = 'FirstName',
    LAST_NAME = 'LastName',
    USER_NAME = 'UserName',
    PASSWORD = 'Password',
    E_MAIL = 'Email',
    CELL_PHONE = 'Mobilephone'
}

export class User {
    public firstName: string;
    public lastName: string;
    public userName: string;
    public password: string;
    public email: string;
    public cellPhone: string;
    public role: string;
    public customer: string;

    public set(inputName: InputFieldName, value: string) {
        switch (inputName) {
            case InputFieldName.FIRST_NAME:
                this.firstName = value;
                break;
            case InputFieldName.LAST_NAME:
                this.lastName = value;
                break;
            case InputFieldName.USER_NAME:
                this.userName = value;
                break;
            case InputFieldName.PASSWORD:
                this.password = value;
                break;
            case InputFieldName.E_MAIL:
                this.email = value;
                break;
            case InputFieldName.CELL_PHONE:
                this.cellPhone = value;
                break;
            default:
                break;
        }
    }
}