import {$, browser, element, by, ElementFinder, until, Key, ExpectedConditions} from "protractor";
import {String} from "typescript-string-operations";

export class WebTablesPage {

    private url: string;
    private addFinder: ElementFinder;
    private tableDataXpath: string;

    constructor() {
        this.url = 'http://www.way2automation.com/angularjs-protractor/webtables/';
        this.addFinder = $("button[type='add']")
        this.tableDataXpath = "((//tr[contains(@ng-repeat, 'dataRow')])[{0}]/td)[{1}]";
    }

    public openBrowser() {
        browser.get(this.url);
    }

    async clickOnAddUserLink() {
        await this.addFinder.click();
    }

    async getDataFromTheFirstLineOfTheTable(dataName: UserDataTableIndex) {
        let value: number = dataName;
        let xpath = String.Format(this.tableDataXpath, 1, value);
        return await element(by.xpath(xpath)).getText();
    }

    async search(text) {
        let searchBox = await $("input[placeholder=Search]");
        await searchBox.sendKeys(text);
        await browser.actions().sendKeys(Key.ENTER).perform();
        until.elementTextContains(searchBox, text);
    }

    async clickOnDeleteButtonOfTheFirstResult(name) {
        let buttonXpath = "//tr/td[contains(text(),'{0}}')]//parent::tr//child::button[contains(@ng-click,'delUser')]"
        buttonXpath = String.Format(buttonXpath, name)
        let removeItenValidatorButton = await element(by.xpath(buttonXpath));
        until.elementIsVisible(removeItenValidatorButton)
        await removeItenValidatorButton.click();
    }

    async confirmDeletion() {
        let okBtn = await $("button[text=OK]");
        until.elementIsVisible(okBtn);
        await okBtn.click();
    }

    async theUserMustNotBeInTheResults(name) {
        console.log('You must ignore the following print exception.')
        let validation = false
        try {
            this.clickOnDeleteButtonOfTheFirstResult(name)
        } catch (e) {
            validation = true
        }
        return validation;
    }

    isLoaded() {
        until.elementIsVisible(this.addFinder);
    }

}

export enum UserDataTableIndex {
    FIRST_NAME = 1,
    LAST_NAME = 2,
    USER_NAME = 3,
    CUSTOMER = 5,
    ROLE = 6,
    EMAIL = 7,
    CELL_PHONE = 8
}
