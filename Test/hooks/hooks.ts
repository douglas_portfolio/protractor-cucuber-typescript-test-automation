const { BeforeAll, After, AfterAll, Status } = require("cucumber");
import * as fs from "fs";
import { config } from "../steps/config";
import {browser, element, by, ElementFinder, ExpectedConditions} from "protractor";
import {String, StringBuilder} from 'typescript-string-operations';
import { Given, When, Then } from "cucumber";

export var chai = require('chai');
export var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
export var expect = chai.expect;

BeforeAll({timeout: 100 * 1000}, async () => {
    await browser.get(config.baseUrl);
});

After(async function(scenario) {
    if (scenario.result.status === Status.FAILED) {
        // screenShot is a base-64 encoded PNG
         const screenShot = await browser.takeScreenshot();
         this.attach(screenShot, "image/png");
    }
});

AfterAll({timeout: 100 * 1000}, async () => {
    await browser.quit();
});

export var selectDropdownbyNum = function ( element, optionNum ) {
    if (optionNum){
        var options = element.all(by.tagName('option'))
            .then(function(options){
                options[optionNum].click();
            });
    }
};