Feature: WebTables Automation

  As an Engineer Candidate
  I need to automate the WebTables Page
  So that I should show my automation capabilities

  @OutlineScenario
  Scenario Outline: Add a user and validate the user has been added to the table
    Given I go to the webtables page
    When I click on Add User link
    Then The add user popup was showed
    When I fill the "FIRST_NAME" field with <FirstName>
    When I fill the "LAST_NAME" field with <LastName>
    When I fill the "USER_NAME" field with <UserName>
    When I fill the "PASSWORD" field with <Password>
    When I fill the "E_MAIL" field with <Email>
    When I fill the "CELL_PHONE" field with <CellPhone>
    When I choose the customer <Customer>
    When I select the role <Role>
    When I click on the save button
    Then The user must be showed in the table

    Examples:
      | FirstName | LastName      | UserName   | Password   | Email             | CellPhone  | Customer      | Role       |
      | 'Joe'     | 'Hilberstein' | 'joe.hilb' | 'jhilb999' | 'jhilb@gmail.com' | '11115566' | 'Company AAA' | 'Customer' |

  @CucumberScenario
  Scenario: Delete user User Name: novak and validate user has been deleted
    Given I go to the webtables page
    When I type 'novak' in the search box
    When I click on delete button of the first result containing 'novak'
    When I confirm the deletion
    When I type 'novak' in the search box
    Then The 'novak' user must not be in the results