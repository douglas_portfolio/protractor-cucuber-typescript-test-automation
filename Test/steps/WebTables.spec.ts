import {Given, Then, When} from "cucumber";
import {UserDataTableIndex, WebTablesPage} from "../pages/WebTables.po";
import {AddUserPopupPo, InputFieldName, RoleDropDownIndex} from "../pages/AddUserPopup.po";
import {expect} from "../hooks/hooks";
import {browser} from "protractor";

//Used Pages
const webtablesPage = new WebTablesPage();
const addUserPopupPage = new AddUserPopupPo();

//Specs
Given('I go to the webtables page', async () => {
    await webtablesPage.openBrowser();
});

When('I click on Add User link', async () => {
    await webtablesPage.clickOnAddUserLink();
});

Then('The add user popup was showed', () => {
    expect(addUserPopupPage.isLoaded(), true);
});

When('I fill the {string} field with {string}', async (inputFieldName: string, data: string) => {
    let input = InputFieldName[inputFieldName];
    await addUserPopupPage.fillInputWithName(input, data);
});

When('I choose the customer {string}', async (customerDropDownName) => {
    await addUserPopupPage.clickOnChosenCustomer(customerDropDownName);
})

When('I select the role {string}', async (roleOption) => {
    let role = RoleDropDownIndex[roleOption];
    await addUserPopupPage.selectRole(role);
});

When('I click on the save button', async () => {
    await addUserPopupPage.clickOnSave();
});

Then('The user must be showed in the table', async () => {
    webtablesPage.isLoaded();
    let user = addUserPopupPage.user;
    let firstName = webtablesPage.getDataFromTheFirstLineOfTheTable(UserDataTableIndex.FIRST_NAME);
    let lastName = webtablesPage.getDataFromTheFirstLineOfTheTable(UserDataTableIndex.LAST_NAME);
    let email = webtablesPage.getDataFromTheFirstLineOfTheTable(UserDataTableIndex.EMAIL);
    let cellphone = webtablesPage.getDataFromTheFirstLineOfTheTable(UserDataTableIndex.CELL_PHONE);
    let username = webtablesPage.getDataFromTheFirstLineOfTheTable(UserDataTableIndex.USER_NAME);
    let customer = webtablesPage.getDataFromTheFirstLineOfTheTable(UserDataTableIndex.CUSTOMER);
    let role = webtablesPage.getDataFromTheFirstLineOfTheTable(UserDataTableIndex.ROLE);
    expect(firstName).to.eventually.equal(user.firstName);
    expect(lastName).to.eventually.equal(user.lastName);
    expect(email).to.eventually.equal(user.email);
    expect(cellphone).to.eventually.equal(user.cellPhone);
    expect(username).to.eventually.equal(user.userName);
    expect(customer).to.eventually.equal(user.customer);
    expect(role).to.eventually.equal(user.role);

});

When('I type {string} in the search box', async (text) => {
    webtablesPage.search(text);
});

When('I click on delete button of the first result containing {string}', async (name) => {
    webtablesPage.clickOnDeleteButtonOfTheFirstResult(name);
});

When('I confirm the deletion', async () => {
    webtablesPage.confirmDeletion();
});

Then('The {string} user must not be in the results', async (name) => {
    expect(webtablesPage.theUserMustNotBeInTheResults(name)).to.eventually.equal(true);
})
